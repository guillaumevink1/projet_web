<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator') {
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - account management </title>
	</head>
	<body>
		<?php
			include('../header.php');
			/*echo '<pre>';
			print("</br> SESSION "); print_r($_SESSION);
			print("</br> POST "); print_r($_POST);
			print("</br> SERVER "); print_r($_SERVER);
			echo '</pre>';*/
		?>
		<div class='inner-body' id='account-management-page'>

		<section class='pop-section'>
			<?php 
				if (isset($_POST['deletion']) AND $_POST['deletion']['btn'] == 'Delete the accounts') {
					if (isset($_POST['indexselection'])) {
						print("<div class='dialog'>");
						print("<form method='POST' action='accountManagement.php'>");
						print("<p> Do you really want to delete these accounts ? </p>");
						print("<ul>");
						foreach($_POST['indexselection'] as $index) {
							$mail = $_POST['selection']['mail'][$index];
							print("<li> $mail </li>");
							print("<input id='$mail' type='hidden' value='$mail' name='selection[mail][]'/>");
						}
						print("</ul>");
						print("<div class='form-confirmation'>");
						print("</br> <button type='submit' name='deletion[btn]' value='conf-admin' class='reset'> Yes </button>");
						print("</br> <input type='submit' name='deletion[btn]' value='No' class='submit'/>");
						print("</div>");
						print("</form>");
						print("</div>");
					} else {
						print("<div class='failure'> No account selected </div>");
					}
				}

				if (isset($_POST['edition']) AND $_POST['edition']['btn'] == 'Edit a profile') {
					if (isset($_POST['indexselection'])) {
						if (count($_POST['indexselection']) == 1) {
							header('Location: accountEditionFormByAdmin.php');
							$index = $_POST['indexselection'][0]; 
							$_SESSION['management']['cat'] = $_POST['selection']['cat'][$index]; 
							$_SESSION['management']['first_name'] = $_POST['selection']['first_name'][$index]; 
							$_SESSION['management']['last_name'] = $_POST['selection']['last_name'][$index]; 
							$_SESSION['management']['institut'] = $_POST['selection']['institut'][$index]; 
							$_SESSION['management']['mail'] = $_POST['selection']['mail'][$index]; 
							$_SESSION['management']['country'] = $_POST['selection']['country'][$index];
							$_SESSION['management']['pwd'] = $_POST['selection']['pwd'][$index];
						} else  {
							print("<div class='failure'> Just one account at time for edition </div>");
						} 
					} else {
						print("<div class='failure'> No account selected </div>");
					}
				}

			?>
		</section>

		
		<section id='account-buttons-section'> 

				<form method="POST" action="accountCreationFormByAdmin.php">
					<input type='submit' name="creation[btn]" value='Add a new account' class='submit'/>
				</form>

				<form method="POST" action="accountManagement.php">
					<input type='submit' name="edition[btn]" value='Edit a profile' class='select'/>
					<input type='submit' name="deletion[btn]" value='Delete the accounts' class='reset'/>
				
			
		</section>

		<section id='account-section'>

		<?php

			// récup toutes les info de la table users sauf admin => rangé dans une variable usersTable
			$nbrows = 3; 
			$queryRecup = "SELECT * FROM  `users` WHERE User_Cat != \"administrator\"";
			$resRecup = requestS($queryRecup);
			if (!array_key_exists('error', $resRecup)) {
				displayAccount($resRecup,$nbrows);
			}
			

		?>

			</form>			
		<?php
			pagination($resRecup,$nbrows,"accountManagement.php");
			
		?>

		</section>

		
		</div>
		<?php
			include('../footer.php');
		?>
	</body>
</html>

