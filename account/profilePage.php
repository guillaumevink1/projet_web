<?php
session_start();
include('accountFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) { // problème de test, si on est connecté en tant qu'utilisateur externe, on peut quand meme accéder à cette page, sinon pas de bonne redirection pour la suppr de compte
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - profile </title>
	</head>

	<body>
		<?php
			include('../header.php');
			/*echo '<pre>';
			print("</br> SESSION "); print_r($_SESSION);
			print("</br> POST "); print_r($_POST);
			echo '</pre>';*/
		?>
		<div class='inner-body' id='profile-page'>

		<section class='pop-section'>
			<?php 
				if (isset($_POST['deletion']) AND $_POST['deletion']['btn'] == 'Delete') {
					print("<div class='dialog'> <form method='POST' action='profilePage.php'>");
					print("<p> Do you really want to delete your account ? </p>");
					//$mail = $_SESSION['mail'];
					//print("<input id='$mail' type='hidden' value='$mail' name='selection[mail][]' checked/>");
					print("<div class='form-confirmation'>");
					print("<button type='submit' name='deletion[btn]' value='conf-user' class='reset'> Yes </button>");
					print("<input type='submit' name='deletion[btn]' value='No' class='submit'/>");
					print("</div>");
					print("</form> </div>");
				}

			?>
		</section>

		

		<section id='profile-section'>
			<?php
				switch ($_SESSION['cat']) {
					case 'administrator': $image='user1'; break;
					case 'moderator': $image='user2'; break;
					case 'authority': $image='user3'; break;
					case 'sponsor': $image='user4'; break;
					case 'visitor': $image='user5'; break;
				}
				print("<img class='user-image' src='$image.png' alt='User logo'>");
				if (isset($_SESSION['institut'])) {
					printf("<span class='profile-item'> Institut </span> <span> %s </span>", $_SESSION['institut']);
				}
				if (isset($_SESSION['first_name']) AND isset($_SESSION['last_name'])) {
					printf("<span class='profile-item'> Name </span> <span> %s %s </span>", $_SESSION['first_name'],$_SESSION['last_name']);
				}
				printf("<span class='profile-item'> Mail </span> <span> %s </span>", $_SESSION['mail']);
				printf("<span class='profile-item'> Country </span> <span> %s </span>", $_SESSION['country']);
			?>
			<div>
				<form method="POST" action="accountOwnEditionForm.php" id='edit-btn-form'>
					<input type='submit' name="edition[btn]" value='Edit my profile' id='edit-btn' class='submit' <?php if ($_SESSION['cat']=='authority') {print("disabled");}?>/>
				</form>
				<form method="POST" action="profilePage.php" id='delete-btn-form'>
					<input type='submit' name="deletion[btn]" value='Delete' id='delete-btn' class='reset' <?php if ($_SESSION['cat']=='authority') {print("disabled");}?>/>
				</form>
			</div>
		</section>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>