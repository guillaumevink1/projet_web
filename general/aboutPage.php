<?php
session_start();
include('../functions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
} 
if (!isset($_SESSION['cat'])) {
	$_SESSION['cat'] = 'ext';
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - about </title>
	</head>
	<body>
		<?php
			include('../header.php');
		?>
		<div class='inner-body centered'id='about-page'>

		<section>
			<h1> ClinicalTrialsByMLG </h1>
			<p> Our objective is to facilitate access to information relating to the various diagnostic and therapeutic means, in order to improve their understanding, to participate in research, and to help transparency. It is for this purpose that ClinicalTrialsByMLG allows the general public to consult the various clinical studies carried out during the development of these new means, and the various institutes behind these studies to put them online very simply and quickly. </p>
			<h2> Authors: </h2>
			<p> Manon Carvalho </br> Loic Jelensperger </br> Guillaume Vink </p>
		</section>
		
		</div>
		<?php
			include('../footer.php');
		?>
	</body>
</html>