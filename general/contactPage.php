<?php
session_start();
include('../functions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
} 
if (!isset($_SESSION['cat'])) {
	$_SESSION['cat'] = 'ext';
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - contact </title>
	</head>
	<body>
		<?php
			include('../header.php');
		?>
		<div class='inner-body centered' id='contact-page'>

		<section>
			<p>For any questions, comments or concerns please contact us at the following adress : <br/> contact@bymlg.com </p>
			<p>Location : <br/> Polytech Nice Sophia <br/> 930 Route des Colles <br/> 06410 Biot, France</p>
		</section>

		</div>
		<?php
			include('../footer.php');
		?>
	</body>
</html>