# Modules	
import mysql.connector
from matplotlib.ticker import FuncFormatter
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
# Script de generation de graphe pour le site web

def parse_database():
	conn = mysql.connector.connect(host="127.0.0.1",user="manon", password="MCP*phpMyAdmin", database="testweb")
	cursor=conn.cursor()
	sql = cursor.execute("""SELECT * FROM reports""")
	rows=cursor.fetchall()
	total = []
	date = []
	validated =[]
	refused=[]
	ongoing=[]
	planified=[]
	male=[]
	female=[]
	both=[]
	cat1=[]
	cat2=[]
	cat3=[]
	cat4=[]
	for row in rows:
		date.append(str(row[0]))
		total.append(row[1])
		validated.append(row[2])
		refused.append(row[3])
		ongoing.append(row[4])
		planified.append(row[5])
		male.append(row[6])
		female.append(row[7])
		both.append(row[8])
		cat1.append(row[9])
		cat2.append(row[10])
		cat3.append(row[11])
		cat4.append(row[12])
	return(date,total,validated,refused,ongoing,planified,male,female,both,cat1,cat2,cat3,cat4)
	
	
	
def graphe_bar(x,y,name,xlabel,title):
	fig = plt.figure()
	plt.bar(x,y)
	plt.xticks(x)
	plt.title(title)
	plt.xlabel(xlabel)
	plt.ylabel("Number of studies")
	plt.savefig(name)



def graphe_status_evol(x,valid,ref,ongo,plan):
	fig =plt.figure()
	header=['validated','refused','ongoin','planified']
	
	#Concert in numpy array
	data1=np.array(valid)
	data2=np.array(ref)
	data3=np.array(ongo)
	data4=np.array(plan)
	
	N=len(x)
	width=0.25
	ind = np.arange(N)
	
	#create bar stacked
	p1=plt.bar(ind,valid,width,color='r')
	p2=plt.bar(ind,ref,width,bottom=data1,color='black')
	p3=plt.bar(ind,ongo,width,bottom=data1+data2, color='yellow')
	p4=plt.bar(ind,plan,width,bottom=data1+data2+data3,color='b')
	
	# Legend the graph
	plt.title("Evolution of the studies status")
	plt.ylabel("Study number")
	plt.xlabel("Date of reports")
	plt.xticks(ind,x)
	plt.legend((p1[0],p2[0],p3[0],p4[0]),(header[0],header[1],header[2],header[3]))
	plt.savefig('graphes/status_evol.png')
	
	
	
	
	
	

#main
#parse the data from the database
data=parse_database()
graphe_bar(data[0],data[1],'graphes/total.png',"date of reports","Evolution of the studies stored on the website" )

#select information form the first line
status=[data[2][-1],data[3][-1],data[4][-1],data[5][-1]]
ticks=["validated","refused","ongoing","planified"]
graphe_bar(ticks,status,'graphes/status.png', "Study's status", "Status of the studies at "+data[0][-1])

graphe_status_evol(data[0],data[2],data[3],data[4],data[5])

gender=[data[6][-1],data[7][-1],data[8][-1]]
ticks=["male","female","both"]
graphe_bar(ticks,gender,'graphes/gender.png',"Patient's gender","M/F repartition at "+data[0][-1])

patients=[data[7][-1],data[8][-1],data[9][-1],data[10][-1]]
ticks = ["0 to 50","50 to 250","250 to 1000","more than 1000"]
graphe_bar(ticks,patients,'graphes/patients.png', "Number of patients","Number of patients at "+data[0][-1] )


