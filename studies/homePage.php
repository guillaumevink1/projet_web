<?php
session_start();
include('studiesFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
}
if (!isset($_SESSION['cat'])) {
	$_SESSION['cat'] = 'ext';
}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - Home </title>
	</head>
    <body>
    	<?php 
      		include('../header.php');
      		/*echo '<pre>';
			print("</br> SESSION "); print_r($_SESSION);
			print("</br> POST "); print_r($_POST);
			echo '</pre>';*/
		?>
      <div class='inner-body' id='home-page'>

		<form method="POST" action="homePage.php">

<!-- Conteneur pour le trie et la recherche -->

		<section class="sorting-and-research-section">

<!-- Conteneur pour le trie -->
			<div id="sorting" class="sorting"> 
				<h1 class='filter-title'> Sort by </h1>
				<div class='sorting-criteria-div'>
				<label> 1st sorting criteria </label>
				<select name="1st_sorting_criteria" class="sorting-criteria">
					<option value="Start_Date"
						<?php if (isset($_POST['1st_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['1st_sorting_criteria']=="Start_Date") {print("selected");}?>
						> Date </option>
					<option value="Status"
						<?php if (isset($_POST['1st_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['1st_sorting_criteria']=="Status") {print("selected");}?>
						> Status </option>
					<option value="Patient_Nb"
						<?php if (isset($_POST['1st_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['1st_sorting_criteria']=="Patient_Nb") {print("selected");}?>
						> Patient number </option>
					<option value="Duration"
						<?php if (isset($_POST['1st_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['1st_sorting_criteria']=="Duration") {print("selected");}?>
						> Duration </option>
				</select>
				</div>
				<div class='sorting-criteria-div'>
				<label> 2nd sorting criteria </label>
				<select name="2nd_sorting_criteria" class="sorting-criteria">
					<option value=""> Nothing </option>
					<option value="Start_Date"
						<?php if (isset($_POST['2nd_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['2nd_sorting_criteria']=="Start_Date") {print("selected");}?>
						> Date </option>
					<option value="Status"
						<?php if (isset($_POST['2nd_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['2nd_sorting_criteria']=="Status") {print("selected");}?>
						> Status </option>
					<option value="Patient_Nb"
						<?php if (isset($_POST['2nd_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['2nd_sorting_criteria']=="Patient_Nb") {print("selected");}?>
						> Patient number </option>
					<option value="Duration"
						<?php if (isset($_POST['2nd_sorting_criteria']) AND !isset($_POST['filter-reset']) AND $_POST['2nd_sorting_criteria']=="Duration") {print("selected");}?>
						> Duration </option>
				</select>
				</div>
			</div>

<!-- Conteneur pour la recherche -->
			<div id="searchbar" class="searchbar">
				<h1 class='filter-title'> Search </h1>
				<input type=text name="keyword" placeholder="Search by Keyword" 
					value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['keyword']);} ?>" />
			</div>
<!-- Conteneur pour les boutons -->
			<div id="Apply-or-Reset" class="form-confirmation">
				<input type="submit" name="filter-submit" value="Submit" class='submit'>
				<input type="submit" name="filter-reset" value ="Reset" class='reset'>
			</div>

		</section>

<!-- Conteneur pour les filtres et les études -->
		<div class='filter-and-study-section'>

<!-- Conteneur pour les filtres -->
		<section class="filter-section">
		<h1 class='filter-title'> Filter </h1>
		<div id="demographic-data" class="filter-category">
			<input type="checkbox" name="accordion[]" value="check1" id="check1" class="filter-category-checkbox" 
				<?php if (isset($_POST['accordion']) AND !isset($_POST['filter-reset']) AND in_array("check1", $_POST['accordion'])) {print("checked");}?> >
			<label class="filter-category-label" for="check1">Demographic data</label>
			<div class="filter-category-content">
				<h2 class='filter-title'> Gender: </h2>
					<input type="radio" name="gender" value="Both" 
						<?php if (isset($_POST['gender']) AND !isset($_POST['filter-reset']) AND $_POST['gender']=="Both") {print("checked");}?>
						/> Both
					<input type="radio" name="gender" value="Male" 
						<?php if (isset($_POST['gender']) AND !isset($_POST['filter-reset']) AND $_POST['gender']=="Male") {print("checked");}?>
						/> M
					<input type="radio" name="gender" value="Female" 
						<?php if (isset($_POST['gender']) AND !isset($_POST['filter-reset']) AND $_POST['gender']=="Female") {print("checked");}?>
						/> F
				<h2 class='filter-title'> Age: </h2>
					<input type="checkbox" name="age[]" value="0-19"
						<?php if (isset($_POST['age']) AND !isset($_POST['filter-reset']) AND in_array("0-19", $_POST['age'])) {print("checked");}?>
						/> 0-19
					<input type="checkbox" name="age[]" value="20-39"
						<?php if (isset($_POST['age']) AND !isset($_POST['filter-reset']) AND in_array("20-39", $_POST['age'])) {print("checked");}?>
						/> 20-39						
					<input type="checkbox" name="age[]" value="40-59"
						<?php if (isset($_POST['age']) AND !isset($_POST['filter-reset']) AND in_array("40-59", $_POST['age'])) {print("checked");}?>
						/> 40-59
					<input type="checkbox" name="age[]" value="60-200"
						<?php if (isset($_POST['age']) AND !isset($_POST['filter-reset']) AND in_array("60-200", $_POST['age'])) {print("checked");}?>
						> 60+
				<h2 class='filter-title'> Group size: </h2>
					<input type="number" name="groupe_size_min" min="0" 
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['groupe_size_min']);} ?>" /> to
					<input type="number" name="groupe_size_max" min="0" 
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['groupe_size_max']);} ?>"/>
			</div>
		</div>
		<div id="study" class="filter-category">
			<input type="checkbox" name="accordion[]" value="check2" id="check2" class="filter-category-checkbox" 
				<?php if (isset($_POST['accordion']) AND !isset($_POST['filter-reset']) AND in_array("check2", $_POST['accordion'])) {print("checked");}?> >
			<label class="filter-category-label" for="check2">Study</label>
			<div class="filter-category-content">
				<h2 class='filter-title'> Starting date: </h2>
					<input type="date" name="beginning_date" 
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['beginning_date']);} ?>" /> to
					<input type="date" name="ending_date"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['ending_date']);} ?>" /> 
				<h2 class='filter-title'> Country: </h2>
					<input type="text" name="country_of_study"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['country_of_study']);} ?>"/>
				<h2 class='filter-title'> Status: </h2>
					<input type="checkbox" name="status[]" value="planified"
						<?php if (isset($_POST['status']) AND !isset($_POST['filter-reset']) AND in_array("planified", $_POST['status'])) {print("checked");}?>
						/> planified
					<input type="checkbox" name="status[]" value="ongoing"
						<?php if (isset($_POST['status']) AND !isset($_POST['filter-reset']) AND in_array("ongoing", $_POST['status'])) {print("checked");}?>
						/> ongoing
					<br>
					<input type="checkbox" name="status[]" value="validated"
						<?php if (isset($_POST['status']) AND !isset($_POST['filter-reset']) AND in_array("validated", $_POST['status'])) {print("checked");}?>
						/> validated
					<input type="checkbox" name="status[]" value="refused"
						<?php if (isset($_POST['status']) AND !isset($_POST['filter-reset']) AND in_array("refused", $_POST['status'])) {print("checked");}?>
						/> refused
				<h2 class='filter-title'> Phase: </h2>
					<input type="checkbox" name="phase[]" value="0"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("0", $_POST['phase'])) {print("checked");}?>
						/> 0 
					<input type="checkbox" name="phase[]" value="1"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("1", $_POST['phase'])) {print("checked");}?>
						/> 1
					<input type="checkbox" name="phase[]" value="2"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("2", $_POST['phase'])) {print("checked");}?>
						/> 2
					<input type="checkbox" name="phase[]" value="3"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("3", $_POST['phase'])) {print("checked");}?>
						/> 3
					<input type="checkbox" name="phase[]" value="4"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("4", $_POST['phase'])) {print("checked");}?>
						/> 4
					<input type="checkbox" name="phase[]" value="Not applicable"
						<?php if (isset($_POST['phase']) AND !isset($_POST['filter-reset']) AND in_array("Not applicable", $_POST['phase'])) {print("checked");}?>
						/> Not applicable
				<h2 class='filter-title'> Duration: </h2>
					<input type="number" name="duration_min" min="0"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['duration_min']);} ?>"/> to
					<input type="number" name="duration_max" min="0"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['duration_max']);} ?>"/>
					<select name="time_type">
						<option value="days" 
							<?php if (isset($_POST['time_type']) AND !isset($_POST['filter-reset']) AND $_POST['time_type']=="days") {print("selected");}?> 
							> days </option>
						<option value="months"
							<?php if (isset($_POST['time_type']) AND !isset($_POST['filter-reset']) AND $_POST['time_type']=="months") {print("selected");}?>
							> months </option>
						<option value="years"
							<?php if (isset($_POST['time_type']) AND !isset($_POST['filter-reset']) AND $_POST['time_type']=="years") {print("selected");}?>
							> years </option>
					</select>
				<h2 class='filter-title'> Author: </h2>
					<input type="text" name="author"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['author']);} ?>"/>
			</div>
		</div>
		<div id="treatment" class="filter-category">
			<input type="checkbox" name="accordion[]" value="check3" id="check3" class="filter-category-checkbox" 
				<?php if (isset($_POST['accordion']) AND !isset($_POST['filter-reset']) AND in_array("check3", $_POST['accordion'])) {print("checked");}?> >
			<label class="filter-category-label" for="check3">Treatment</label>
			<div class="filter-category-content">
				<h2 class='filter-title'> Molecule name: </h2>
					<input type="text" name="molecule_name"
						value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['molecule_name']);} ?>"/>
				<h2 class='filter-title'> Route of administration: </h2>
					<input type="checkbox" name="administration[]" value="oral" id='administration-oral'
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("oral", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-oral'> oral </label>
					<input type="checkbox" name="administration[]" value="cutaneous" id='administration-cutaneous' 
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("cutaneous", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-cutaneous'> cutaneous </label>
					<input type="checkbox" name="administration[]" value="injection" id='administration-injection' 
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("injection", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-injection'> injection </label> <br>
					<input type="checkbox" name="administration[]" value="respiratory" id='administration-respiratory'
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("respiratory", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-respiratory'> respiratory </label>
					<input type="checkbox" name="administration[]" value="occulary" id='administration-occulary'
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("occulary", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-occulary'> occulary </label>
					<input type="checkbox" name="administration[]" value="other" id='administration-other'
						<?php if (isset($_POST['administration']) AND !isset($_POST['filter-reset']) AND in_array("other", $_POST['administration'])) {print("checked");}?>
						/> <label for='administration-other'> other </label>
			</div>
		</div>
		<div id="disease" class="filter-category">
			<input type="checkbox" name="accordion[]" value="check4" id="check4" class="filter-category-checkbox" 
				<?php if (isset($_POST['accordion']) AND !isset($_POST['filter-reset']) AND in_array("check4", $_POST['accordion'])) {print("checked");}?> >
			<label class="filter-category-label" for="check4">Disease</label>
			<div class="filter-category-content">
			<h2 class='filter-title'> Type: </h2>
				<select name="disease_type" style="max-width:25%;">
					<option value="">---</option>
					<option value="infectious and parasitic diseases"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="infectious and parasitic diseases") {print("selected");}?>
						>infectious and parasitic diseases</option>
					<option value="neoplasms"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="neoplasms") {print("selected");}?>
						>neoplasms</option>
					<option value="endocrine nutritional and metabolic diseases and immunity disorders"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="endocrine nutritional and metabolic diseases and immunity disorders") {print("selected");}?>
						>endocrine nutritional and metabolic diseases <br> and immunity disorders</option>
					<option value="diseases of the blood and blood-forming organs"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the blood and blood-forming organs") {print("selected");}?>
						>diseases of the blood and blood-forming organs</option>
					<option value="mental disorders"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="mental disorders") {print("selected");}?>
						>mental disorders</option>
					<option value="diseases of the nervous system and sense organs"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the nervous system and sense organs") {print("selected");}?>
						>diseases of the nervous system and sense organs</option>
					<option value="diseases of the circulatory system"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the circulatory system") {print("selected");}?>
						>diseases of the circulatory system</option>
					<option value="diseases of the respiratory system"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the respiratory system") {print("selected");}?>
						>diseases of the respiratory system</option>
					<option value="diseases of the digestive system"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the digestive system") {print("selected");}?>
						>diseases of the digestive system</option>
					<option value="diseases of the genitourinary system"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the genitourinary system") {print("selected");}?>
						>diseases of the genitourinary system</option>
					<option value="complications of pregnancy childbirth and the puerperium"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="complications of pregnancy childbirth and the puerperium") {print("selected");}?>
						>complications of pregnancy childbirth and the puerperium</option>
					<option value="diseases of the skin and subcutaneous tissue"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the skin and subcutaneous tissue") {print("selected");}?>
						>diseases of the skin and subcutaneous tissue</option>
					<option value="diseases of the musculoskeletal system and connective tissue"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="diseases of the musculoskeletal system and connective tissue") {print("selected");}?>
						>diseases of the musculoskeletal system and connective tissue</option>
					<option value="congenital anomalies"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="congenital anomalies") {print("selected");}?>
						>congenital anomalies</option>
					<option value="certain conditions originating in the perinatal period"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="certain conditions originating in the perinatal period") {print("selected");}?>
						>certain conditions originating in the perinatal period</option>
					<option value="symptoms signs and ill-defined conditions"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="symptoms signs and ill-defined conditions") {print("selected");}?>
						>symptoms signs and ill-defined conditions</option>
					<option value="injury and poisoning"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="injury and poisoning") {print("selected");}?>
						>injury and poisoning</option>
					<option value="other"
						<?php if (isset($_POST['disease_type']) AND !isset($_POST['filter-reset']) AND $_POST['disease_type']=="other") {print("selected");}?>
						>other</option>
				</select>
			<h2 class='filter-title'> Name: </h2>
				<input type="text" name="disease_name"
					value="<?php if(isset($_POST['filter-submit']) AND !isset($_POST['filter-reset'])){print($_POST['disease_name']);} ?>"/>
			<h2 class='filter-title'> Stage/Condition: </h2>
				<select name="disease_stage">
					<option value="">---</option>
					<option value="early" 
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="early") {print("selected");}?>
						> early </option>
					<option value="flare-up" 
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="flare-up") {print("selected");}?>
						> flare-up </option>
					<option value="progressive" 
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="progressive") {print("selected");}?> 
						> progressive </option>
					<option value="refractory"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="refractory") {print("selected");}?>
						> refractory </option>
					<option value="acute"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="acute") {print("selected");}?>
						> acute </option>
					<option value="chronic"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="chronic") {print("selected");}?>
						> chronic </option>
					<option value="clinical"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="clinical") {print("selected");}?>
						> clinical </option>
					<option value="subclinical"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="subclinical") {print("selected");}?>
						> subclinical </option>
					<option value="cure"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="cure") {print("selected");}?>
						> cure </option>
					<option value="terminal"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="terminal") {print("selected");}?>
						> terminal </option>
					<option value="other"
						<?php if (isset($_POST['disease_stage']) AND !isset($_POST['filter-reset']) AND $_POST['disease_stage']=="other") {print("selected");}?>
						> other </option>
				</select>
			</div>
		</div>
		<div id="Apply-or-Reset" class="form-confirmation">
			<input type="submit" name="filter-submit" value ="Submit" class='submit'>
			<input type="submit" name="filter-reset" value ="Reset" class='reset'>
		</div>
		</form>
		</section>

<!-- Conteneur pour l'affichage des études -->
		<section class="studies-section">
			<?php
				/* Ajout des nouveaux favoris */
				if(isset($_POST['bookmarkSubmit'])){ //Si le bouton "Add to my bookmarks" a été cliqué.
					if (isset($_POST['bookmarks'])) {
						foreach($_POST['bookmarks'] as $bookmark){
							addBookmark($bookmark,$_SESSION['idUser']);
						}
					}
				}


				/* Affichage des études */
				$nbrows = 4; 
				if(empty($_POST['filter-submit'])){	//Si l'utilsateur n'a pas encore rempli le formulaire (première ouverture de la page).
					$table_studies = requestS("SELECT * FROM studies ORDER BY Start_Date DESC");
					 //On affiche par défaut toutes les études classées par date décroissant.
				}
				else{
					$table_studies = searchStudies();
				}
				displayStudies($table_studies,$nbrows);
				pagination($table_studies,$nbrows,"homePage.php");
				
			?>
		</section>

		</div>

		</div>
		<?php
			include('../footer.php');
		?>
    </body>

</html>