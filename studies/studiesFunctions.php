<?php   
   include('../functions.php');


   function transformDurationIntoDays($duration,$timetype){
		/* fonction qui transforme une durée donnée en son équivalent en jour, en fonction du type de durée (jour,mois ou année) */
		if ($timetype == "days"){ return ($duration);}
		if ($timetype == "months"){ return ($duration*30);} // 1 month = 30 days
		if ($timetype == "years"){return ($duration*365);} // 1 year = 365 days
		else{ echo "Error: No valid timetype.";}
	}
	function searchStudies() {
		/* fonction qui écrit la requête d'études dans la BDD en fonction des filtres, tris et recherche par mot clé. */
		$study_request_table = "SELECT * FROM studies ";
		$study_request_filter = "";
		$study_request_sorting = "";
		//Partie filtre
		if (!empty($_POST['gender'])){
			$study_request_filter .= "AND Gender = '".$_POST['gender']."' ";	 
		}
		if (!empty($_POST['age'])){
			foreach($_POST['age'] as $age_value){
				$age_value_array=explode('-',$age_value);
				$study_request_filter .= "AND (Age_Min OR Age_Max BETWEEN $age_value_array[0] AND $age_value_array[1]) ";
			}	// Le moindre recouvrement entre les 2 intervalles (celui du filtre et celui de l'étude) suffit pour que l'étude soit récupérée dans la requête.
		}
		if (!empty($_POST['groupe_size_min'])){
			$study_request_filter .= "AND Patient_Nb >= ".$_POST['groupe_size_min']." ";
		}
		if (!empty($_POST['groupe_size_max'])){
			$study_request_filter .= "AND Patient_Nb <= ".$_POST['groupe_size_max']." ";
		}
		if (!empty($_POST['beginning_date'])){
			$study_request_filter .= "AND Start_date >= '".$_POST['beginning_date']."' ";
		}
		if (!empty($_POST['ending_date'])){
			$study_request_filter .= "AND Start_date <= '".$_POST['ending_date']."' ";
		}
		if (!empty($_POST['country_of_study'])){
			$study_request_filter .= "AND Country = '".$_POST['country_of_study']."' ";
		}
		if (!empty($_POST['status'])){
			$study_request_filter .= "AND (";
			foreach($_POST['status'] as $status_value){
				$study_request_filter .= "Status = '".$status_value."' OR ";
			}
			$study_request_filter = substr($study_request_filter,0,-4);	// suppression du dernier "OR "
			$study_request_filter .= ") ";
		}
		if (!empty($_POST['phase'])){
			$study_request_filter .= "AND (";
			foreach($_POST['phase'] as $phase_value){
				$study_request_filter .= "phase = '".$phase_value."' OR ";
			}
			$study_request_filter = substr($study_request_filter,0,-4);	// suppression du dernier "OR "
			$study_request_filter .= ") ";
		}
		$time_type = $_POST['time_type'];
		if (!empty($_POST['duration_min'])){
			$duration_min_day = transformDurationIntoDays($_POST['duration_min'],$time_type);
			$study_request_filter .= "AND Duration >= ".$duration_min_day." ";
		}
		if (!empty($_POST['duration_max'])){
			$duration_max_day = transformDurationIntoDays($_POST['duration_max'],$time_type);
			$study_request_filter .= "AND Duration <= ".$duration_max_day." ";
		}
		if (!empty($_POST['author'])){
			$study_request_filter .= "AND Author = '".$_POST['author']."' ";
		}
		if (!empty($_POST['molecule_name'])){
			$study_request_filter .= "AND Treatment_Mol = '".$_POST['molecule_name']."' ";
		}
		if (!empty($_POST['administration'])){
			$study_request_filter .= "AND (";
			foreach($_POST['administration'] as $administration_value){
				$study_request_filter .= "Admin1 = '".$administration_value."' OR Admin2 = '".$administration_value."' OR Admin3 = '".$administration_value."' OR ";
			}
			$study_request_filter = substr($study_request_filter,0,-4);	// suppression du dernier "OR " 
			$study_request_filter .= ") ";
		}
		if (!empty(($_POST['disease_type']))){
			$study_request_filter .= "AND Disease_Type = '".$_POST['disease_type']."' ";
		}
		if(!empty($_POST['disease_name'])){
			$study_request_filter .= "AND Disease = '".$_POST['disease_name']."' ";
		}
		if (!empty(($_POST['disease_stage']))){
			$study_request_filter .= "AND Disease_Stage = '".$_POST['disease_stage']."' ";
		}
		//Partie recherche par mot clé. Elle complète la partie filtre. Nous sommes encore dans la clause WHERE.
		if (!empty($_POST['keyword'])){
			$kword= $_POST['keyword'];
			$study_request_filter .= "AND Title LIKE '%".$kword."%' OR Institut LIKE '%".$kword."%' OR Author LIKE '%".$kword.
			"%' OR Abstract LIKE '%".$kword."%' OR Disease LIKE '%".$kword."%' OR Treatment_Mol LIKE '%".$kword."%' ";
		}
		//Modification de la chaine de caractère pour obtenir le bon format
		if(!empty($study_request_filter)){
			$study_request_filter = substr($study_request_filter,4);	// suppression du premier "AND "
			$study_request_filter = "WHERE ".$study_request_filter; // ajout du WHERE en début de clause
		}
		//Partie tri
		$study_request_sorting .= "ORDER BY ".$_POST['1st_sorting_criteria']." DESC"; //1er critère de tri toujours défini
		if(!empty($_POST['2nd_sorting_criteria'])){
			$study_request_sorting .= ",".($_POST['2nd_sorting_criteria'])." DESC";
		}
		$complete_study_request = $study_request_table.$study_request_filter.$study_request_sorting;
		//print($complete_study_request); ligne à décommenter pour afficher et vérifier la requête SQL
		$tableResStudies = requestS($complete_study_request);
		return $tableResStudies;
	}
	
	function addBookmark($idStudy,$idUser){
		$resBookmarkInsertion = requestTF("INSERT INTO bookmarks VALUES (".$idUser.",'".$idStudy."')");
		if (!empty($resBookmarkInsertion)){
			return $resBookmarkInsertion; //retourne le message d'erreur de la BDD si jamais il y en a un.
		} 
	}
	function isInBookmarks($idStudy,$idUser){
		//Vérifie pour l'utilisateur connecté si une étude donnée est déjà dans la table bookmarks.
		$resIsInBookmarks = requestS("SELECT '.$idStudy.' FROM bookmarks WHERE (idStudy = '".$idStudy."' AND idUser = ".$idUser.")");
		return (count($resIsInBookmarks) == 1); //Retourne TRUE si le bookmark existe déjà. 
	}  //count($resIsInBookmarks) ne peut pas être supérieur à 1 grâce à la contrainte d'unicité dans la BDD.

	function deleteBookmark($idStudy,$idUser){
		$resBookmarkDeletion = requestTF("DELETE FROM bookmarks WHERE (idStudy = '".$idStudy."' AND idUser = ".$idUser.")");
		if (!empty($resBookmarkDeletion)){
			echo $resBookmarkD; //Imprime le message d'erreur de la BDD si jamais il y en a un.
		} 
	}


	function displayStudies($table,$nbrows) {
		if (isset($_GET['page'])) {
			$actu = $_GET['page'];
		} else {
			$actu =1;
		}
		$offset = ($actu-1)*$nbrows;
		$end = $offset+$nbrows-1;
		$size = count($table);
		if ($end > $size - 1) {$end = $size - 1;}
		if(count($table) == 0)
			echo "<section class='pop-section'><div class='dialog'><span> Sorry, no results match your request </span></div></section>";
		else{
			echo "<table class='studies-table'>";
			echo "<thead> <tr>";
			print("<th> Studies </th>");
			if (in_array($_SESSION['cat'], ['visitor','sponsor','authority'], TRUE)){ //Si l'utilisateur est un visiteur un commanditaire ou une autorité,
				echo "<form method='POST' action='homePage.php'>"; 
				echo "<th> <input type='submit' name='bookmarkSubmit' value='Add to my Bookmarks' class='select'> </th>"; //alors on donne la possibilité d'ajouter des favoris.
			} 
			echo "<tr> </thead>";
			echo "<tbody>";
			for ($i = $offset; $i <= $end; $i++) {
				echo "<tr>";
				$study = $table[$i];
				$idStudy = $study["idStudy"];
				//Décision d'autoriser ou pas l'accès à l'étude
				$have_access = false;
				if(empty($study['idRestriction'])){// s'il n'y a pas de restriction
					$have_access = true;
				}
				else{//s'il y a une restriction
					if(in_array($_SESSION['cat'], ['moderator','administrator','authority'], TRUE)){
						$have_access = true;
					}
					else{
						$req="SELECT Restriction_Status as status FROM restrictions WHERE idRestriction=".$study['idRestriction'];
						$result=requestS($req);
						$status = $result[0]['status'];
						if(in_array($status,['not treated yet','accepted'])){
							if($_SESSION['cat']!='ext'){
								$sponsorID = $study['idUser'];
								if ($_SESSION['idUser'] == $sponsorID){ //Si on est le commanditaire
									$have_access = true;  //alors on a accès à sa propre étude.
								}
								else{
									$req2="SELECT idUser FROM restrictionlist WHERE idStudy = '".$idStudy."'";
									$users_who_have_access = requestS($req2);
									foreach($users_who_have_access as $key => $value){
										if($_SESSION['idUser'] == $value['idUser']){
											$have_access = true;
										}
									}
								}
							}
						}else{ // Si la demande de restricton est refusée
							$have_access = true;
						}
					}
				}
				
				echo "<td class='study-cell'>";
				if($have_access){
					studyPreview($study);
				}
				else{
					studyPreviewRestricted($study);
				}
				echo "</td>";
				if (in_array($_SESSION['cat'], ['visitor','sponsor','authority'], TRUE) AND $have_access == true){//accès aux bookmarks
					echo "<td class='check-cell'> ";
					if (isInBookmarks($idStudy,$_SESSION['idUser']))
						echo "<input id='bookmark-checkbox$i' type='checkbox' disabled class='bookmark-checkbox'/>";
					else
						echo "<input id='bookmark-checkbox$i' name='bookmarks[]' value=$idStudy type='checkbox' class='bookmark-checkbox'/>";
					echo "<label class='bookmark-label' for='bookmark-checkbox$i'></label>";
					echo "</td>";
				}
			
				echo "</tr>";
			}
			echo "</tbody>";
			echo "</table>";
		}
		
	}

	function displayBookmarks($table,$nbrows) {
		/* La table d'entrée est la table des bookmarks d'un utilisateur donné */
		if (isset($_GET['page'])) {
			$actu = $_GET['page'];
		} else {
			$actu =1;
		}
		$offset = ($actu-1)*$nbrows;
		$end = $offset+$nbrows-1;
		$size = count($table);
		if ($end > $size - 1) {$end = $size - 1;}
		if(count($table) == 0)
			echo "<section class='pop-section'><div class='dialog'><span> You have no bookmarks yet </span></div></section>";
		else{
			echo "<table class='studies-table'>";
			echo "<form method='POST' action='bookmarkManagement.php'>";
			/*echo "<thead> <tr>";
			echo "<th> My bookmarks ★ </th>";
			echo "<th>"; 
			echo "<input type='submit' name='bookmarkRemoval' value='Remove from my Bookmarks' class='select'> </th>"; 
			echo "<tr> </thead>";*/
			echo "<tbody>";
			for ($i = $offset; $i <= $end; $i++) {
				$line = $table[$i];
				$idStudy = $line["idStudy"];
				echo "<tr>";
				echo "<td class='study-cell'>";
				studyPreview($line);
				echo "</td>";
				echo "<td class='check-cell'>";
				echo "<input name='bookmarks[selection][]' value=$idStudy type='checkbox' class='bookmark-delete-checkbox'/>";
				echo "</td>";
				echo "</tr>";
			}
			echo "</tbody>";
			echo "<tfoot> <tr> <td></td> <td class='check-cell'> <input type='submit' name='bookmarks[btn]' value='Remove from my Bookmarks' class='select'> </td> </tr> </tfoot>";
			echo "</form>";
			echo "</table>";
		}
	}

	function studyPreviewRestricted($line){ 
		/* Cette fonction permet d'afficher la prévisualisation réduite d'une étude dans un tableau, 
		pour les personnes qui n'y ont pas accès. Légère modification par rapport à studyPreview() dans functions.php .
		 */
		$start_date = $line["Start_Date"];
		$title = $line["Title"];
		if(strlen($title) > 90){
			$title = substr($title,0,90)."...";
		}
		

		echo $start_date." - ".$title."<br>"; 
		echo "<i> The access to this study is restricted.</i> <br>";
		if($_SESSION['cat']=='ext'){
			echo "<i>Try to Log In to see if you have access to it. </i>";
		}else{
			echo "<i>You have no access to it.</i>";
		}
	}
?>