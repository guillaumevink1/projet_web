

		<section id='study-section'>
			<aside>

				<?php if ($page == "oneStudyPage") { ?>
				<form method="POST" action="<?php $current_page ?>"> 
					<?php
						if (isset($_SESSION['cat']) AND ($_SESSION['cat'] == 'visitor' OR $_SESSION['cat'] == 'sponsor' OR $_SESSION['cat'] == 'authority')) {
							if (!isInBookmarks($study_information['idStudy'],$_SESSION['idUser'])) {
								print("<button type='submit' name=bookmarks[btn] value='add-one' class='submit'> Add to my bookmarks </button> ");
							}
						}

						if (isset($_POST['bookmarks']['btn'])) {
							if ($_POST['bookmarks']['btn'] == 'add-one') {
								addBookmark($study_information['idStudy'],$_SESSION['idUser']);
								$loc = basename($current_page);
								header("Location: $loc");
							}
						}
					?>
				</form>
				<?php } ?>
				
				<h1> <?php print($study_information['Title']) ;?> </h1>
				<span>International ID: <?php print($study_information['idStudy']) ;?></span>
				<span>Start date: <?php print($study_information['Start_Date']) ;?></span>
				<span>Status: <?php print($study_information['Status']) ;?></span>
				<?php if(isset($study_information['Duration'])){
					print("<span>Duration: ".$study_information['Duration']." days </span>");
				}?>
				<?php if(isset($study_information['Institut'])){
					print("<span>Institute: ".$study_information['Institut']." </span>");
				}?>
				<?php if(isset($study_information['Nb_Street'])){
					print("<span>Complete adress: ".$study_information['Nb_Street'].", ".$study_information['Country']."</span>");
				}?>
				<span>Trial phase: <?php print($study_information['Phase']) ;?></span>
				<?php if(isset($study_information['Author'])){
					print("<span>Author: ".$study_information['Author']." </span>");
				}?>
				<span>weblink: <a href=<?php print($study_information['Weblink']);?> ><?php print($study_information['Weblink']);?></a></span>

				<h2> Demographic data </h2>
				<span>Gender: <?php print($study_information['Gender']) ;?></span>
				<span>Number of patient enrolled: <?php print($study_information['Patient_Nb']) ;?></span>
				<span>Age: from  <?php print($study_information['Age_Min'])?> to <?php print($study_information['Age_Max'])?> years old </span>

				<h2> Disease </h2>
				<span>Disease name: <?php print($study_information['Disease']); ?> </span>
				<span>Disease type: <?php print($study_information['Disease_Type']); ?> </span>
				<span>Disease stage: <?php print($study_information['Disease_Stage']); ?> </span>
				
				<h2> Treatment </h2>
				<span>Treatment molecule: <?php print($study_information['Treatment_Mol']); ?> </span>
				<?php if(isset($study_information['Treatment_Target'])){
					print("<span>Treatment target: ".$study_information['Treatment_Target']."</span>");
				}?>
				<span>1st option of administration:</span>
				<ul>
					<li> Route of administration : <?php print($study_information['Admin1']); ?> </li>
					<?php if(isset($study_information['Dose_Min1']) && isset($study_information['Dose_Max1']) && isset($study_information['Unit1'])){
						print("<li> Dose range :  from ".$study_information['Dose_Min1']." to ".$study_information['Dose_Max1']." ".$study_information['Unit1']."</li>");
					}?>
				</ul>
				<?php if ((isset($study_information['Admin2']) && $study_information['Admin2']!="") || (isset($study_information['DoseMin2']) && isset($study_information['DoseMax2']) && isset($study_information['Unit2']))) { ?>
					<span>2nd option of administration:</span>	
					<ul>				
					<?php if (isset($study_information['Admin2'])) { ?>
						<li> Route of administration : <?php print($study_information['Admin2']); ?> </li>
					<?php } if(isset($study_information['Dose_Min2']) && isset($study_information['Dose_Max2']) && isset($study_information['Unit2'])){
						print("<li> Dose range :  from ".$study_information['Dose_Min2']." to ".$study_information['Dose_Max2']." ".$study_information['Unit2']."</li>");
					}?>
					</ul>
				<?php } ?>
				<?php if ((isset($study_information['Admin3']) && $study_information['Admin3']!="") || (isset($study_information['Dose_Min3']) && isset($study_information['Dose_Max3']) && isset($study_information['Unit3']))) { ?>
					<span>3rd option of administration:</span>	
					<ul>				
					<?php if (isset($study_information['Admin3'])) { ?>
						<li> Route of administration : <?php print($study_information['Admin3']); ?> </li>
					<?php } if(isset($study_information['Dose_Min3']) && isset($study_information['Dose_Max3']) && isset($study_information['Unit3'])){
						print("<li> Dose range :  from ".$study_information['Dose_Min3']." to ".$study_information['Dose_Max3']." ".$study_information['Unit3']."</li>");
					}?>
					</ul>
				<?php } ?>
				

				<h2> Abstract </h2>
					<span><?php print($study_information['Abstract']); ?> </span>

			</aside>

			<article>
				<iframe id="pdfetude" src="../studiesPDF/<?php print($idStudy);?>.pdf"></iframe>
			</article>
			
		</section>

