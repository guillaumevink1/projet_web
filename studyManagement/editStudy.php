<?php
session_start();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'sponsor') {
	header('Location: ../studies/homePage.php');
}
include('studyManagementFunctions.php');
$dose_units=array('µg','mg','g','µg/mL','mg/mL','g/L','µg/kg','mg/kg','g/kg','mg/min','mg/h','g/h','mL/h','ppm');
$administration_routes=array('oral','cutaneaous','injection','respiratory','occulary','other');
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - Study submission form </title>
		
	</head>
	<body>
	<?php	include('../header.php');	?>
	<div class='inner-body study-form-page' id="study-form-page">
		<?php
			if(isset($_POST['idStudy'])){
				$req="SELECT * FROM studies WHERE idStudy='".$_POST['idStudy']."'";
				$res=requestS($req);
			}
		?>
	<section id="study-form-title-section" class="study-form-title-section">
		
		<h1> Study submission </h1>
		<a href='studyJsonForm.php' > Studies submission with Json File </a>

	</section>

	<section id='study-form-section'>
		<form method="POST" action="studyManagement.php" enctype="multipart/form-data" class="form-stlye-1"> 
		
		<fieldset id="study-fieldset"> <legend> Study </legend>
			
			<label for="idStudy"> <span> International ID <span class="required">*</span> </span>
			<input id="idStudy" name="idStudy" value="<?php print($res[0]['idStudy'])?>" readonly/> 
				<?php 
					if (isset($error['uniqueID'])) {
							print("<p class='text-error'>".$error['uniqueID']."</p>");
					} 
				?>
			</label>

			<label> <span> Trial Status <span class="required">*</span> : </span>
				<label class="check-label"><input name="Status" type="radio" value="planified" <?php if($res[0]['Status']=="planified"){print("checked");}?>  required/> planified</label>
				<label class="check-label"><input name="Status" type="radio" value="ongoing" <?php if($res[0]['Status']=="ongoin"){print("checked");}?> /> ongoing</label>
				<label class="check-label"><input name="Status" type="radio" value="validated" <?php if($res[0]['Status']=="validated"){print("checked");}?> /> validated</label>
				<label class="check-label"><input name="Status" type="radio" value="refused" <?php if($res[0]['Status']=="refused"){print("checked");}?> /> refused</label>
			</label>

			<label for="Start_Date"> <span> Start date <span class="required">*</span> </span>
			<input id="Start_Date" type="date" name="Start_Date" value="<?php print($res[0]['Start_Date'])?>" required/></label>

			<label for="End_Date"> <span> End date </span>
			<input id="End_Date" type="date" name="End_Date" value="<?php print($res[0]['End_Date'])?>"/></label>

			<label for="Title"> <span> Title <span class="required">*</span> </span>
			<input id="Title" name="Title" value="<?php print($res[0]['Title'])?>" disabled/></label>

			<label class="location"> <span> Fill at least one location <span class="required">*</span> :</span><br>

			<ul>
			<li><label for="Institut" class="location"> <span> Institute </span> 
				<input id="Institut"  name="Institut" value="<?php print($res[0]['Institut'])?>" disabled/>
			</label></li> 

			<li><label for="Country" class="location"> <span> Adress </span> 
				<input id="Country" name="Country" placeholder="Country" value="<?php print($res[0]['Country'])?>" />
				<input id="Nb_Street" name="Nb_Street" placeholder="City, Street, Number" value="<?php print($res[0]['Nb_Street'])?>" />
			</label></li>
			</ul>
				<?php 
					if (isset($error['Location'])) {
							print("<p class='text-error'>".$error['Location']."</p>");
					} 
				?>
	
			</label>

			<label > <span> Trial phase <span class="required">*</span> : </span> 
				<label class="check-label"><input  type="radio" name="Phase" value="0" required <?php if($res[0]['Phase']=="0"){print("checked");}?> />0</label>
				<label class="check-label"><input  type="radio" name="Phase" value="1" <?php if($res[0]['Phase']=="1"){print("checked");}?> />1</label>
				<label class="check-label"><input  type="radio" name="Phase" value="2" <?php if($res[0]['Phase']=="2"){print("checked");}?> />2</label>
				<label class="check-label"><input  type="radio" name="Phase" value="3" <?php if($res[0]['Phase']=="3"){print("checked");}?> />3</label>
				<label class="check-label"><input  type="radio" name="Phase" value="4" <?php if($res[0]['Phase']=="4"){print("checked");}?> />4</label>
				<label class="check-label"><input  type="radio" name="Phase" value="Not applicable" <?php if($res[0]['Phase']=="Not applicable"){print("checked");}?> />Not applicable</label>
			</label>

			<label for="Author"> <span> Author </span>
			<input id="Author"  name="Author" value="<?php print($res[0]['Author'])?>"/></label>

			<label for="Weblink"> <span> Trial weblink <span class="required">*</span> </span> 
			<input id="Weblink" type="url" name="Weblink" value="<?php print($res[0]['Weblink'])?>" required/></label>

		</fieldset>

		<fieldset id="disease-fieldset"> <legend> Disease </legend>

			<label for="Disease"> <span> Disease name <span class="required">*</span> </span> 
			<input id="Disease" name="Disease" value="<?php print($res[0]['Disease'])?>" disabled/></label>

			<label for="Disease_Type"> <span> Disease type <span class="required">*</span> </span> 
			<select id="Disease_Type" name="Disease_Type" disabled>
					<option value="infectious and parasitic diseases">infectious and parasitic diseases</option>
					<option value="neoplasms" <?php if($res[0]['Disease_Type']=="neoplasms"){print("selected");}?> >neoplasms</option>
					<option value="endocrine nutritional and metabolic diseases and immunity disorders" <?php if($res[0]['Disease_Type']=="endocrine nutritional and metabolic diseases and immunity disorders"){print("selected");}?> >endocrine nutritional and metabolic diseases <br> and immunity disorders</option>
					<option value="diseases of the blood and blood-forming organs" <?php if($res[0]['Disease_Type']=="diseases of the blood and blood-forming organs"){print("selected");}?> >diseases of the blood and blood-forming organs</option>
					<option value="mental disorders" <?php if($res[0]['Disease_Type']=="mental disorders"){print("selected");}?> >mental disorders</option>
					<option value="diseases of the nervous system and sense organs" <?php if($res[0]['Disease_Type']=="diseases of the nervous system and sense organs"){print("selected");}?> >diseases of the nervous system and sense organs</option>
					<option value="diseases of the circulatory system" <?php if($res[0]['Disease_Type']=="diseases of the circulatory system"){print("selected");}?> >diseases of the circulatory system</option>
					<option value="diseases of the respiratory system" <?php if($res[0]['Disease_Type']=="diseases of the respiratory system"){print("selected");}?> >diseases of the respiratory system</option>
					<option value="diseases of the digestive system" <?php if($res[0]['Disease_Type']=="diseases of the digestive system"){print("selected");}?> >diseases of the digestive system</option>
					<option value="diseases of the genitourinary system" <?php if($res[0]['Disease_Type']=="diseases of the genitourinary system"){print("selected");}?> >diseases of the genitourinary system</option>
					<option value="complications of pregnancy childbirth and the puerperium" <?php if($res[0]['Disease_Type']=="complications of pregnancy childbirth and the puerperium"){print("selected");}?> >complications of pregnancy childbirth and the puerperium</option>
					<option value="diseases of the skin and subcutaneous tissue" <?php if($res[0]['Disease_Type']=="diseases of the skin and subcutaneous tissue"){print("selected");}?> >diseases of the skin and subcutaneous tissue</option>
					<option value="diseases of the musculoskeletal system and connective tissue" <?php if($res[0]['Disease_Type']=="diseases of the musculoskeletal system and connective tissue"){print("selected");}?> >diseases of the musculoskeletal system and connective tissue</option>
					<option value="congenital anomalies" <?php if($res[0]['Disease_Type']=="congenital anomalies"){print("selected");}?> >congenital anomalies</option>
					<option value="certain conditions originating in the perinatal period" <?php if($res[0]['Disease_Type']=="certain conditions originating in the perinatal period"){print("selected");}?> >certain conditions originating in the perinatal period</option>
					<option value="symptoms signs and ill-defined conditions" <?php if($res[0]['Disease_Type']=="symptoms signs and ill-defined conditions"){print("selected");}?> >symptoms signs and ill-defined conditions</option>
					<option value="injury and poisoning" <?php if($res[0]['Disease_Type']=="injury and poisoning"){print("selected");}?> >injury and poisoning</option>
					<option value="other" <?php if($res[0]['Disease_Type']=="other"){print("selected");}?> >other</option>

				</select>
			</label>

			<label for="Disease_Stage"> <span> Disease Stage <span class="required">*</span> </span> 
			<select id="Disease_Stage" name="Disease_Stage" disabled>
				<option value="early" <?php if($res[0]['Disease_Stage']=="early"){print("selected");}?> >early</option>
				<option value="flare-up" <?php if($res[0]['Disease_Stage']=="flare-up"){print("selected");}?> >flare-up</option>
				<option value="progressive" <?php if($res[0]['Disease_Stage']=="progressive"){print("selected");}?> >progressive</option>
				<option value="refractory" <?php if($res[0]['Disease_Stage']=="refractory"){print("selected");}?> >refractory</option>
				<option value="acute" <?php if($res[0]['Disease_Stage']=="acute"){print("selected");}?> >acute</option>
				<option value="chronic" <?php if($res[0]['Disease_Stage']=="chronic"){print("selected");}?> >chronic</option>
				<option value="clinical"<?php if($res[0]['Disease_Stage']=="clinical"){print("selected");}?> >clinical</option>
				<option value="subclinical"<?php if($res[0]['Disease_Stage']=="subclinical"){print("selected");}?> >subclinical</option>
				<option value="cure" <?php if($res[0]['Disease_Stage']=="cure"){print("selected");}?> >cure</option>
				<option value="terminal"<?php if($res[0]['Disease_Stage']=="terminal"){print("selected");}?> >terminal</option>
				<option value="other"<?php if($res[0]['Disease_Stage']=="other"){print("selected");}?> >other</option>
			</select>
			</label>
		</fieldset>

		<fieldset id="demographic-fieldset"> <legend> Demographic data </legend>

			<label for="Age_Min"> <span> Patient's minimum age <span class="required">*</span> </span> 
			<input id="Age_Min" type="number" name="Age_Min" value="<?php print($res[0]['Age_Min'])?>" required/></label>

			<label for="Age_Max"> <span> Patient's maximum age <span class="required">*</span> </span> 
			<input id="Age_Max" type="number" name="Age_Max" value="<?php print($res[0]['Age_Max'])?>" required/></label>

			<label> <span> Patients gender <span class="required">*</span> : </span> 
				<label class="check-label"><input type="radio" name="gender" value="Male" <?php if($res[0]['Gender']=="Male"){print("checked");}?> required/>Male</label>
				<label class="check-label"><input type="radio" name="gender" value="Female" <?php if($res[0]['Gender']=="Female"){print("checked");}?> />Female</label>
				<label class="check-label"><input type="radio" name="gender" value="Both" <?php if($res[0]['Gender']=="Both"){print("checked");}?> />Both</label>
			</label>

			<label for="Patient_Nb"> <span> Number of people enrolled <span class="required">*</span> </span> 
			<input id="Patient_Nb" type="number" name="Patient_Nb" value="<?php print($res[0]['Patient_Nb'])?>" max='10000000000' required/></label>

		</fieldset>

		<fieldset id="treatment-fieldset"> <legend> Treatment </legend>

			<label for="Treatment_mol"> <span> Treatment (molecule) <span class="required">*</span> </span> 
			<input id="Treatment_mol" name="Treatment_Mol" value="<?php print($res[0]['Treatment_Mol'])?>" required/></label>

			<label for="Treatment_target"> <span> Treatment's target </span>
			<input id="Treatment_target" type="Treatment_target" name="Treatment_Target" value="<?php print($res[0]['Treatment_Target'])?>"/></label>

			<div id="administration-table-div"> 

			<label> <span> 1st option of administration <span class="additional-info">(mandatory)</span> </span>
				<label for="Admin1" class="check-label"> <span class="check-label"> Route of administration <span class="required">*</span> </span>
				<select name="Admin1" id="Admin1">
					<?php foreach($administration_routes as $route){
						if ($res[0]['Admin1'] == "$route") {
							print("<option value=$route selected>$route</option>");
						} else {
							print("<option value=$route>$route</option>");
						}
					} ?>
				</select> </label>
				<label for="Dose_Min1" class="check-label"> Minimal dose 
				<input id="Dose_Min1" type="number" name="Dose_Min1" value="<?php print($res[0]['Dose_Min1'])?>"/></label>
				<label for="Dose_Max1" class="check-label"> Maximal dose 
				<input id="Dose_Max1" type="number" name="Dose_Max1" value="<?php print($res[0]['Dose_Max1'])?>"/></label>
				<label for="Unit1" class="check-label"> Units's dose 
				<select name="Unit1" id="Unit1">
					<?php foreach($dose_units as $dose_unit){
						if ($res[0]['Unit1'] == "$dose_unit") {
							print("<option value=$dose_unit selected>$dose_unit</option>");
							$sel1 = TRUE;
						} else {
							print("<option value=$dose_unit>$dose_unit</option>");
						}
					} ?>
					<option value ='' <?php if (!isset($sel1)) {print("selected");} ?> >---</option>
				</select></label>
			</label>

			<label> <span> 2nd option of administration <span class="additional-info">(optionnal)</span> </span>
				<label for="Admin2" class="check-label"> Route of administration
				<select name="Admin2" id="Admin2">
					<?php foreach($administration_routes as $route){
						if ($res[0]['Admin2'] == "$route") {
							print("<option value=$route selected>$route</option>");
						} else {
							print("<option value=$route>$route</option>");
						}
					} ?>
					<option value ='' selected>---</option>
				</select> </label>
				<label for="Dose_Min2" class="check-label"> Minimal dose
				<input id="Dose_Min2" type="number" name="Dose_Min2" value="<?php print($res[0]['Dose_Min2'])?>"/></label>
				<label for="Dose_Max2" class="check-label"> Maximal dose
				<input id="Dose_Max2" type="number" name="Dose_Max2" value="<?php print($res[0]['Dose_Max2'])?>"/></label>
				<label for="Unit2" class="check-label"> Units's dose
				<select name="Unit2" id="Unit2">
					<?php foreach($dose_units as $dose_unit){
						if ($res[0]['Unit2'] == "$dose_unit") {
							print("<option value=$dose_unit selected>$dose_unit</option>");
							$sel2 = TRUE;
						} else {
							print("<option value=$dose_unit>$dose_unit</option>");
						}
					} ?>
					<option value ='' <?php if (!isset($sel2)) {print("selected");} ?> >---</option>
				</select></label>
			</label>

			<label> <span> 3rd option of administration <span class="additional-info">(optionnal)</span> </span>
				<label for="Admin3" class="check-label"> Route of administration
				<select name="Admin3" id="Admin3">
					<?php foreach($administration_routes as $route){
						if ($res[0]['Admin3'] == "$route") {
							print("<option value=$route selected>$route</option>");
						} else {
							print("<option value=$route>$route</option>");
						}
					} ?>
					<option value ='' selected>---</option>
				</select> </label>
				<label for="Dose_Min3" class="check-label"> Minimal dose
				<input id="Dose_Min3" type="number" name="Dose_Min3" value="<?php print($res[0]['Dose_Min3'])?>"/></label>
				<label for="Dose_Max3" class="check-label"> Maximal dose
				<input id="Dose_Max3" type="number" name="Dose_Max3" value="<?php print($res[0]['Dose_Max3'])?>"/></label>
				<label for="Unit3" class="check-label"> Units's dose
				<select name="Unit3" id="Unit3">
					<?php foreach($dose_units as $dose_unit){
						if ($res[0]['Unit3'] == "$dose_unit") {
							print("<option value=$dose_unit selected>$dose_unit</option>");
							$sel3 = TRUE;
						} else {
							print("<option value=$dose_unit>$dose_unit</option>");
						}
					} ?>
					<option value ='' <?php if (!isset($sel3)) {print("selected");} ?> >---</option>
				</select></label>
			</label>

			</div>

		</fieldset>

		<fieldset id="general-fieldset"> <legend> General </legend>

			<label for="study_pdf"> <span> Study file<span class="required">*</span> <span class="additional-info">(pdf only, max size: 10MB)</span> </span> 
			<input id="study_pdf" name="study_pdf" type="file" accept="application/pdf">
				<?php 
					if (isset($error['file'])) {
							print("<p class='text-error'>".$error['file']."</p>");
					} 
				?>
			</label>

			<label for="Abstract"> <span> Abstract <span class="required">*</span> </span> 
			<textarea id="Abstract" name="Abstract" cols="40" rows="5" style="vertical-align:top" "" required> <?php print($res[0]['Abstract'])?> </textarea></label>

		</fieldset>

			<span> *required fields</span>
			<div class="form-confirmation">
				<input type="Submit" name="studyFormUpdate" value="submit" class="submit">
			</div>
			<?php
			?>
		</form>
	</section>
	</div>
	<?php include('../footer.php');?>
	</body>
	

</html>

