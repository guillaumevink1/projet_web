<?php 
include('../functions.php');
?>


<?php
// tuto format json : 	https://www.numelion.com/utiliser-manipuler-fichier-json-php.html


/* Ce fichier comporte les fonctions importantes concernant la lecture et l'importation des data via le format json */
/* Prends en entree un fichier au format json et extrait son contenue pour le stocker dans une variable */ 


/*Insert PDF et redirection restriction après sousmission finale*/
	if (isset($_POST['studyJsonFinalSubmit'])){
		$dossier="../studiesPDF/";
		for ($i=0; $i < count($_FILES['pdf']['tmp_name']) ; $i++) { 
			$idStudy=$_SESSION['restriction']['idStudies'][$i];
			move_uploaded_file($_FILES['pdf']['tmp_name'][$i], $dossier.$idStudy.".pdf");
		}
		if($_POST['restriction'] == "Yes"){
			header("Location: restrictionForm.php");
		}
		if($_POST['restriction'] == "No"){
			header("Location: studyManagement.php");
		}
	}





function parseJson($file){
	$jsonFile = file_get_contents($file);
	$data = json_decode($jsonFile, true);
	return($data);
}

function check_data($table){
	$error=0;
	foreach ($table as $key => $study) {
		if(array_key_exists('idStudy',$study)==FALSE or empty($study['idStudy'])){
			$error++;
		}
		elseif(array_key_exists('Status',$study)==FALSE or empty($study['Status'])){
			$error++;
		}
		elseif(array_key_exists('Start_Date',$study)==FALSE or empty($study['Start_Date'])){
			$error++;
		}
		elseif(array_key_exists('Title',$study)==FALSE or empty($study['Title'])){
			$error++;
		}
		elseif(array_key_exists('Phase',$study)==FALSE or empty($study['Phase'])){
			$error++;
		}
		elseif(array_key_exists('Abstract',$study)==FALSE or empty($study['Abstract'])){
			$error++;
		}
		elseif(array_key_exists('Institut',$study)==FALSE || empty($study['Institut'])) {
			$error++;
		}
		elseif(array_key_exists('Weblink',$study)==FALSE or empty($study['Weblink'])){
			$error++;
		}
		elseif(array_key_exists('Disease',$study)==FALSE or empty($study['Disease'])){
			$error++;
		}
		elseif(array_key_exists('Disease_Type',$study)==FALSE or empty($study['Disease_Type'])){
			$error++;
		}
		elseif(array_key_exists('Disease_Stage',$study)==FALSE or empty($study['Disease_Stage'])){
			$error++;
		}
		elseif(array_key_exists('Age_Min',$study)==FALSE or empty($study['Age_Min'])){
			$error++;
		}
		elseif(array_key_exists('Age_Max',$study)==FALSE or empty($study['Age_Max'])){
			$error++;
		}
		elseif(array_key_exists('Gender',$study)==FALSE or empty($study['Gender'])){
			$error++;
		}
		elseif(array_key_exists('Patient_Nb',$study)==FALSE or empty($study['Patient_Nb'])){
			$error++;
		}
		elseif(array_key_exists('Treatment_Mol',$study)==FALSE or empty($study['Treatment_Mol'])){
			$error++;
		}
		elseif(array_key_exists('Administration',$study)==FALSE or empty($study['Administration'])){
			$error++;
		}
		elseif(array_key_exists('Dose_Max',$study)==FALSE or empty($study['Dose_Max'])){
			$error++;
		}
		elseif(array_key_exists('Dose_Min',$study)==FALSE or empty($study['Dose_Min'])){
			$error++;
		}
		elseif(array_key_exists('Dose_Unit',$study)==FALSE or empty($study['Dose_Unit'])){
			$error++;
		}
	}
	if ($error==0){
		$_SESSION['error']['json-bad-field'] = $error;
		return insertJson($table);
	}
	else{
		$_SESSION['error']['json-bad-field'] = $error;
	}
	
}






/* Prends en entree le contenue d'un format json parsé
pour chaque etude contenu dans la variable de stockage, on construit la requete sql d'ajout de donnée et on l'execute 
*/
function insertJson($jsonContent){
	$idsStudies = array();
	$studiesTitles = array();
	$i = 0;
	foreach ($jsonContent as $key => $study) {
		$column = "";
		$values = "";
		foreach ($study as $key => $data) {
			//Traitement du cas particulier des doses, elles sont recupere sous forme de tableau.
			
			if (is_array($data)){
				if($key=="Dose_Min"){
					for ($i=0; $i < count($data); $i++) { 
						$column = $column."Dose_Min".($i+1).",";
						$values = $values."'".$data[$i]."'".",";
					}
				}
				if($key=="Dose_Max"){
					for ($i=0; $i < count($data); $i++) { 
						$column = $column."Dose_Max".($i+1).",";
						$values = $values."'".$data[$i]."'".",";
					}
				}
				if($key=="Administration"){
					for ($i=0; $i < count($data); $i++) { 
						$column = $column."Admin".($i+1).",";
						$values = $values."'".$data[$i]."'".",";
					}
				}
				if($key=="Dose_Unit"){
					for ($i=0; $i < count($data); $i++) { 
						$column = $column."Unit".($i+1).",";
						$values = $values."'".$data[$i]."'".",";
					}
				}
			}
			// Cas General de toutes les autres clefs	et récupération données 
			else{
				if($data != "") {
					$column = $column.$key.",";
					$values = $values."'".$data."'".",";
				}
				if($key == "Start_Date"){
					$start_date = strtotime($data);
				}
				if($key == "End_Date"){
					if(!empty($data)){ $end_date = strtotime($data);}	
				}
				if($key == "idStudy"){
					array_push($idsStudies,$data);
					$idStudy = $data;
				}
				if($key == "Title"){
					array_push($studiesTitles,$data);
				}
			}
		}
		$column = substr($column,0,-1);
		$values = substr($values,0,-1);
		$Sub_Date=date("Y-m-d"); //-La date de soumission
		$Duration = 'NULL';
		if(isset($end_date)){ 
				$Duration=($end_date-$start_date)/60/60/24; //-La durée en jours		
			}
		$query = "INSERT INTO studies (idUSer,Sub_Date,Duration,".$column.") VALUES ('".$_SESSION['idUser']."','".$Sub_Date."',".$Duration.",".$values.")";
		$res = requestTF($query);
		if(empty($res)){
			print("The study with the ID: $idStudy have been added to the database, thank you ! <br>");
		}
		else{
			print("<p class='text-error'> Database error : $res </p>");
			$_SESSION['error']['json_database'] = TRUE;
		}
	}
	$_SESSION['restriction']['idStudies']=$idsStudies;
	$_SESSION['json']['studiesTitles']=$studiesTitles;
}

?>
