<?php
session_start();
include('studyManagementFunctions.php');
testAndSetCookies();
if (!isset($_SESSION['log'])) {
	$_SESSION['log'] = FALSE;
	header('Location: ../studies/homePage.php');
}
if ($_SESSION['cat'] != 'administrator' AND $_SESSION['cat'] != 'moderator' AND $_SESSION['cat'] != 'sponsor') {
	header('Location: ../studies/homePage.php');
}

if (isset($_POST['idsStudy'][0])) {
	$idStudy = $_POST['idsStudy'][0];
}
else {
	$idStudy = $_POST['edit-restriction']['idStudy'];
}
$study_information = requestS("SELECT * FROM studies WHERE idStudy = '$idStudy'");
$study_information=$study_information[0];
$page = pathinfo($_SERVER['REQUEST_URI'], PATHINFO_FILENAME);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> studyAndRestrictionPage </title>
	</head>
	<body>
		<?php include('../header.php'); ?>
		<div class='inner-body study-page' id='study-and-restriction-page'>

		<div class='row-div'>	

		<?php include('../studies/studyPart.php'); ?>
		
		<section id='restriction-view'>

			<section id="restriction-view-title-section">

			<?php 
				$req = "SELECT * FROM restrictions WHERE idRestriction = (SELECT idRestriction FROM studies WHERE idStudy = '".$idStudy."')";
				$restriction_infos = requestS($req);
				$restriction_infos = $restriction_infos[0];
				$req2 = "SELECT Email FROM users WHERE idUser IN (SELECT idUser FROM restrictionlist WHERE idStudy = '".$idStudy."')";
				$email_list = requestS($req2);
				$status = $restriction_infos['Restriction_Status'];
			?>

				<h1> Restriction demand </h1>
				<h2>
				<?php 
				switch ($status) {
					case 'refused':
						print("<span class='refused'> Refused </span>");
						break;
					case 'accepted':
						print("<span class='accepted'> Accepted </span>");
						break;
					case 'not treated yet':
						print("<span class='not_treated'> Not treated yet </span>");
						break;
				}
				?>
				</h2>

			</section>

			<form class="form-style-2">

				<label for="ListeEmail"> List of emails </label>
				<textarea id="ListeEmail" rows="3" cols="50" readonly><?php foreach($email_list as $email){ //obligé d'être sur la même ligne que <textarea> pour ne pas avoir de saut de ligne.
						print($email['Email'].";");
					}?>
				</textarea> 

				<label for="Justification"> Justification's request </label>
				<textarea id="Justification" rows="5" cols="50" readonly><?php print($restriction_infos['Justification']); ?> </textarea> 
			
	<?php	if($restriction_infos['Restriction_Status'] == 'refused') { ?>
				<label> The moderator negative response </label>
				<textarea rows="5" cols="50" readonly><?php print($restriction_infos['Response']);?> </textarea>
		<?php } ?>

			</form>


	<?php if($_SESSION['cat']=='moderator' AND $restriction_infos['Restriction_Status'] == "not treated yet") { ?> 
				
				<form action="studyManagement.php" method="post" class="form-style-2">
					<label> Your response (only necessary if refused) : </label>
					<textarea name="response" rows="5" cols="50" placeholder="refused because..." required></textarea>

					<input type="hidden" name="idRestriction"  value="<?php print($restriction_infos['idRestriction']); ?>">
					<div class="form-confirmation">
						<input type="submit" name="refusedRestriction" value="Refuse" class='reset'>
					
				</form>
				<form action="studyManagement.php" method="post" class="form-style-2">
						<input type="submit" name="acceptedRestriction" value="Accept" class='submit'>
					</div>
					<input type="hidden" name="idRestriction"  value="<?php print($restriction_infos['idRestriction']); ?>">
				</form>
				
	<?php } ?>

		<section class='pop-section'>
			<?php
				if (isset($_SESSION['error']['restriction-no-user']) AND $_SESSION['error']['restriction-no-user']) {
					print("<div class='failure'> No email in our customers </div>");
				}
			?>
		</section>

	<?php if($_SESSION['cat']=='sponsor'AND $restriction_infos['Restriction_Status']=='refused') { ?> 

				<form action="studyAndRestrictionPage.php" method="post" class="form-style-2">
				
					<label> Your new email-list (separated with semicolons): </label>
					<textarea rows="5" cols="50" name="edit-restriction[listeEmail]" placeholder="email1@example.com;email2@example.com;email3@example.com" required></textarea>
				
					<label> Your new justification: </label>
					<textarea name="edit-restriction[Justification]" rows="5" cols="50" ></textarea>

				<div>
					<input type="submit" name="edit-restriction[btn]" value="Send your edited restriction" class='submit'>
				</div>

				<input type="hidden" name="edit-restriction[idStudy]"  value="<?php print($idStudy); ?>">
				<input type="hidden" name="edit-restriction[idRestriction]"  value="<?php print($restriction_infos['idRestriction']); ?>">
				<?php $req = "SELECT idStudy FROM studies WHERE idRestriction = '".$restriction_infos['idRestriction']."'";
				$studyList = requestS($req);
				
				print("<ul class='text-error'>");
				print("Warning: the following studies will be affected by this new restriction : ");	
				foreach ($studyList as $key => $idStudy) {
					print("<li>");
					print("<input type='hidden' name='edit-restriction[idsStudy][]' value='".$idStudy['idStudy']."' />");
					print($idStudy['idStudy']);
					print("</li>");
				}
				print("</ul>");
				?>

				</form>
			<?php } ?>
		</section>

		</div>

		</div>
		<?php include('../footer.php'); ?>	
	</body>
</html>