<?php
	session_start();
	include('jsonFonction.php');
	testAndSetCookies();
	if (!isset($_SESSION['log'])) {
		$_SESSION['log'] = FALSE;
		header('Location: ../studies/homePage.php');
	}
	if ($_SESSION['cat'] != 'sponsor') {
		header('Location: ../studies/homePage.php');
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - Studies submission form </title>
	</head>

	<body>
		<?php
			include('../header.php');
		?>
		<div class= "inner-body study-form-page" id='study-json-form-page'>

		<section id="study-json-form-title-section" class="study-form-title-section">
		
			<h1> Studies submission with Json File </h1>
			<a href='studyForm.php' > Classical study submission </a>

		</section>

		<section id="study-json-form-section">

			<form method="POST" action="studyJsonForm.php" enctype="multipart/form-data" class="form-stlye-1">
				<fieldset> <legend> Json file containing the studies' data </legend>
				<?php 
					if (isset($_FILES['study_json'])){
						$studies = parseJson($_FILES['study_json']['tmp_name']);
						$res = check_data($studies);
						print("<br>");
					if (isset($_SESSION['error']['json-bad-field']) AND $_SESSION['error']['json-bad-field'] > 0 ) {
							print("<p class='text-error'> There is ".$_SESSION['error']['json-bad-field']." error in the json file, some required data are absents or null </p>");
					}
					} if( (!isset(($_FILES['study_json']))) ||(isset($_FILES['study_json']) && (isset($_SESSION['error']['json-bad-field']) || isset($_SESSION['error']['json_database'])))){ ?>
					<input id="study_json" name="study_json" type="file" accept="application/json"> 
					
					<input type="submit" name="studyJsonSubmit" value="OK" class='submit'>
				<?php } ?>
					<p>
						
					</p>
				</fieldset>
			</form>
			
			<?php


			if (isset($_FILES['study_json']) && !isset($_SESSION['error']['json_database']) && $_SESSION['error']['json-bad-field'] == 0){  ?>
				<form method='POST' enctype='multipart/form-data' class='form-stlye-1' id="json-pdf-form">
					<fieldset> <legend> Studies pdf files </legend>
						<div> Pdf file(s) which correspond to the following ID(s) and Title(s): </div>
						<ul>
						<?php
						for ($i=0; $i < count($studies) ; $i++) { 
							$idStudy=$_SESSION['restriction']['idStudies'][$i];
							$title=$_SESSION['json']['studiesTitles'][$i];
							print("<li> <label>"); 
							print("<span> ".$idStudy." - ".$title."</span> <br>");
							print("<input name='pdf[]' type='file' accept='application/pdf' required/>");
							print("</label> </li>"); 
						}
			?>
						</ul>
					</fieldset>

				<fieldset id="restriction-fieldset"> <legend> Restriction </legend>

				<label> <span> Add a restriction list ? <span class="required">*</span> </span> 
					<label class="check-label"><input name="restriction" type="radio" value="Yes" required/>Yes</label>
					<label class="check-label"><input name="restriction" type="radio" value="No"/>No</label>
				</label>
				</fieldset>

				<div class="form-confirmation">
					<input type="submit" name="studyJsonFinalSubmit" value="Submit" class='submit'>
				</div>

			</form>
	<?php
			}
			unset($_SESSION['error']['json-bad-field']);
			unset($_SESSION['error']['json_database']);
			 ?>

		</section>

		</div>
		<?php
			include('../footer.php');
		?>	
	</body>
</html>