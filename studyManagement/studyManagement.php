<?php 
	session_start();

	include('studyManagementFunctions.php');
	testAndSetCookies();
	if (!isset($_SESSION['log'])) {
		$_SESSION['log'] = FALSE;
		header('Location: ../studies/homePage.php');
	}
	if ($_SESSION['cat'] != 'administrator' AND $_SESSION['cat'] != 'moderator' AND $_SESSION['cat'] != 'sponsor') {
		header('Location: ../studies/homePage.php');
	}

?>



<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="../fixstyle.css" />
		<title> ClinicalTrialsByMLG - study management </title>
	</head>
	<body>
		<?php 
			include('../header.php'); 
		?>
		<div class='inner-body' id='study-management-page'>


			<?php if ($_SESSION['cat']=='sponsor') { ?>
			<section id='studies-buttons-section'>
				
				<a href='studyForm.php'> <input type="button" value="Add one study" class='submit'> </a>
				<a href='studyJsonForm.php'> <input type="button" value="Add several studies" class='submit'> </a>
		
			</section>
			<?php } ?>

			<section id='studies-section'>
				<?php
					$nbrows=10;
					if ($_SESSION['cat']=='sponsor') {
						//selectionne toutes les etudes postees par le commanditaire
						$request = "SELECT * FROM studies WHERE idUser =".$_SESSION['idUser']." ORDER BY Sub_Date DESC";
					} else if ($_SESSION['cat']=='administrator' OR $_SESSION['cat']=='moderator') {
						//selectionne toutes les études pour lesquelles une demande de restriction a été faite
						$request = "SELECT * FROM studies JOIN users USING (idUser) WHERE idRestriction IS NOT NULL ORDER BY Sub_Date DESC";
					}
					$data = requestS($request);
					studiesDisplay($data,$nbrows);
				?>

				

				<?php 
					pagination($data,$nbrows,'studyManagement.php') 
				?>
			</section>

		</div>
		<?php 
			include('../footer.php');
		?>
	</body>
</html>

