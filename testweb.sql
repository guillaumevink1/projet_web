-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 06 jan. 2020 à 16:10
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `testweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `idUser` int(11) NOT NULL,
  `idStudy` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bookmarks`
--

INSERT INTO `bookmarks` (`idUser`, `idStudy`) VALUES
(7, 'ACTRN12619001334190'),
(7, 'EUCTR2011-003030-14-IT'),
(7, 'NCT02072057');

-- --------------------------------------------------------

--
-- Structure de la table `reports`
--

CREATE TABLE `reports` (
  `dateReport` date NOT NULL DEFAULT current_timestamp(),
  `studyTotal` int(11) NOT NULL,
  `studyValidated` int(11) NOT NULL,
  `studyRefused` int(11) NOT NULL,
  `studyOngoing` int(11) NOT NULL,
  `studyPlanified` int(11) NOT NULL,
  `male` int(11) NOT NULL,
  `female` int(11) NOT NULL,
  `male_female` int(11) NOT NULL,
  `cat1` int(11) NOT NULL,
  `cat2` int(11) NOT NULL,
  `cat3` int(11) NOT NULL,
  `cat4` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reports`
--

INSERT INTO `reports` (`dateReport`, `studyTotal`, `studyValidated`, `studyRefused`, `studyOngoing`, `studyPlanified`, `male`, `female`, `male_female`, `cat1`, `cat2`, `cat3`, `cat4`) VALUES
('2020-01-04', 3, 0, 0, 0, 3, 0, 0, 3, 1, 1, 0, 1),
('2020-01-05', 3, 0, 0, 0, 3, 0, 0, 3, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `restrictionlist`
--

CREATE TABLE `restrictionlist` (
  `idStudy` varchar(255) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `restrictionlist`
--

INSERT INTO `restrictionlist` (`idStudy`, `idUser`) VALUES
('EUCTR2011-003030-14-IT', 4),
('EUCTR2011-003030-14-IT', 7),
('NCT01269593', 2),
('NCT01269593', 3),
('NCT02072057', 4),
('NCT02072057', 7);

-- --------------------------------------------------------

--
-- Structure de la table `restrictions`
--

CREATE TABLE `restrictions` (
  `idRestriction` int(11) NOT NULL,
  `Justification` text NOT NULL,
  `Restriction_Status` set('not treated yet','accepted','refused') NOT NULL,
  `Response` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `restrictions`
--

INSERT INTO `restrictions` (`idRestriction`, `Justification`, `Restriction_Status`, `Response`) VALUES
(3, 'Only this person should have access to this study beacause of confidential reason.', 'refused', 'this is not a valid argument'),
(7, 'These persons are the reviewers.', 'not treated yet', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `studies`
--

CREATE TABLE `studies` (
  `idStudy` varchar(255) COLLATE utf8_bin NOT NULL,
  `idUser` int(11) NOT NULL,
  `idRestriction` int(11) DEFAULT NULL,
  `Status` set('planified','ongoing','validated','refused') COLLATE utf8_bin NOT NULL,
  `Sub_Date` date NOT NULL,
  `Start_Date` date NOT NULL,
  `End_Date` date DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `Title` text COLLATE utf8_bin NOT NULL,
  `Institut` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Nb_Street` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Phase` set('0','1','2','3','4','Not applicable') COLLATE utf8_bin NOT NULL,
  `Author` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Abstract` text COLLATE utf8_bin NOT NULL,
  `Weblink` text COLLATE utf8_bin NOT NULL,
  `Disease` varchar(255) COLLATE utf8_bin NOT NULL,
  `Disease_Type` set('infectious and parasitic diseases','neoplasms','endocrine nutritional and metabolic diseases and immunity disorders','diseases of the blood and blood-forming organs','mental disorders','diseases of the nervous system and sense organs','diseases of the circulatory system','diseases of the respiratory system','diseases of the digestive system','diseases of the genitourinary system','complications of pregnancy childbirth and the puerperium','diseases of the skin and subcutaneous tissue','diseases of the musculoskeletal system and connective tissue','congenital anomalies','certain conditions originating in the perinatal period','symptoms signs and ill-defined conditions','injury and poisoning','other') COLLATE utf8_bin NOT NULL,
  `Disease_Stage` set('early','flare-up','progressive','refractory','acute','chronic','clinical','subclinical','cure','terminal','other') COLLATE utf8_bin NOT NULL,
  `Age_Min` int(11) NOT NULL,
  `Age_Max` int(11) NOT NULL,
  `Gender` set('Male','Female','Both','') COLLATE utf8_bin NOT NULL,
  `Patient_Nb` int(11) NOT NULL,
  `Treatment_Mol` varchar(255) COLLATE utf8_bin NOT NULL,
  `Treatment_Target` int(11) DEFAULT NULL,
  `Admin1` set('oral','cutaneaous','injection','respiratory','occulary','other') COLLATE utf8_bin NOT NULL,
  `Dose_Min1` decimal(10,0) DEFAULT NULL,
  `Dose_Max1` decimal(10,0) DEFAULT NULL,
  `Unit1` set('µg','mg','g','µg/mL','mg/mL','g/L','µg/kg','mg/kg','g/kg','mg/min','mg/h','g/h','mL/h','ppm','') COLLATE utf8_bin DEFAULT NULL,
  `Admin2` set('oral','cutaneaous','injection','respiratory','occulary','other','') COLLATE utf8_bin DEFAULT NULL,
  `Dose_Min2` decimal(10,0) DEFAULT NULL,
  `Dose_Max2` decimal(10,0) DEFAULT NULL,
  `Unit2` set('µg','mg','g','µg/mL','mg/mL','g/L','µg/kg','mg/kg','g/kg','mg/min','mg/h','g/h','mL/h','ppm','') COLLATE utf8_bin DEFAULT NULL,
  `Admin3` set('oral','cutaneaous','injection','respiratory','occulary','other','') COLLATE utf8_bin DEFAULT NULL,
  `Dose_Min3` decimal(10,0) DEFAULT NULL,
  `Dose_Max3` decimal(10,0) DEFAULT NULL,
  `Unit3` set('µg','mg','g','µg/mL','mg/mL','g/L','µg/kg','mg/kg','g/kg','mg/min','mg/h','g/h','mL/h','ppm','') COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `studies`
--

INSERT INTO `studies` (`idStudy`, `idUser`, `idRestriction`, `Status`, `Sub_Date`, `Start_Date`, `End_Date`, `Duration`, `Title`, `Institut`, `Country`, `Nb_Street`, `Phase`, `Author`, `Abstract`, `Weblink`, `Disease`, `Disease_Type`, `Disease_Stage`, `Age_Min`, `Age_Max`, `Gender`, `Patient_Nb`, `Treatment_Mol`, `Treatment_Target`, `Admin1`, `Dose_Min1`, `Dose_Max1`, `Unit1`, `Admin2`, `Dose_Min2`, `Dose_Max2`, `Unit2`, `Admin3`, `Dose_Min3`, `Dose_Max3`, `Unit3`) VALUES
('ACTRN12619001334190', 6, NULL, 'planified', '2019-07-12', '2020-01-01', NULL, NULL, 'A double-blinded randomised controlled trial of low-dose vs standard dose MDMA in MDMA-assisted therapy for treatment of mood and anxiety symptoms in advanced-stage cancer patients', 'University of Otago', 'New Zealand', NULL, '2', NULL, 'Study_type: Interventional\r\nSimple design: Purpose: Treatment; Allocation: Randomised controlled trial; Masking: Blinded (masking used);Assignment: Parallel;Type of endpoint: Safety/efficacy', 'https://anzctr.org.au/ACTRN12619001334190.aspx', 'Cancer', 'endocrine nutritional and metabolic diseases and immunity disorders', '', 18, 80, 'Both', 24, 'Insuline', NULL, 'oral', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('CTRI-2017-02-007934', 8, NULL, 'ongoing', '2020-01-06', '2016-12-05', NULL, NULL, 'Study to compare x ray and MRI based Brachytherapy in cervical cancer.', 'Tata Memorial Hospita', 'India', ' Mumbai MAHARASHTRA 400012 India Tata Memorial Hospital ', '3', 'Dr Umesh Mahantshetty', 'Conventional Radiography based BT (Controlled Arm) Vs MR-Image Based Brachytherapy (Study Arm) in Locally Advanced Cervical Cancers: A PHASE III Randomized Control Trial', 'http://www.ctri.nic.in/Clinicaltrials/pmaindet2.php?trialid=15094', 'Cancer of the uterine cervix patient', 'infectious and parasitic diseases', 'early', 18, 40, 'Female', 1050, 'X-ray and MRI', NULL, 'injection', '10', '20', 'g/L', 'respiratory', '50', '70', 'mL/h', 'other', '30', '50', ''),
('EUCTR2011-003030-14-IT', 6, 7, 'validated', '2020-01-06', '2012-09-03', '2014-10-07', 764, 'Nerve Growth Factor (NGF) eye drop administration as visual rescue treatment in visual loss-associated optic gliomas.', 'Policlinico Gemelli', 'Italy', 'L.go Gemelli 8', 'Not applicable', NULL, 'Nerve Growth Factor (NGF) eye drop administration as visual rescue treatment in visual loss-associated optic gliomas. - NGF in the optic gliomas', 'https://www.clinicaltrialsregister.eu/ctr-search/search?query=eudract_number:2011-003030-14', 'Glioma', 'neoplasms', 'progressive', 2, 64, 'Both', 24, 'Nerve Growth Factor', 0, 'occulary', '0', '100', 'g/L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('NCT01269593', 6, 3, 'planified', '2020-01-03', '2010-12-29', NULL, NULL, 'PET Imaging of Cancer Patients Using 124I-PUH71: A Pilot Study', 'Memorial Sloan Kettering Cancer Center', 'United State', NULL, '1', 'Mark Dunphy', 'Intervention model\": \"Single Group Assignment\"\"Masking\": \"None (Open Label)\",             \"Primary purpose\": \"Diagnostic\"', 'https://clinicaltrials.gov/show/NCT01269593', 'Cancer', 'neoplasms', 'other', 18, 90, 'Both', 67, '124I-PUH71', NULL, 'oral', NULL, NULL, NULL, '', NULL, NULL, '', '', NULL, NULL, ''),
('NCT01821456', 6, NULL, 'planified', '2020-01-03', '2013-03-19', NULL, NULL, 'Cologne Cohort of Neutropenic Patients (CoCoNut)', NULL, 'Germany', 'KÃ¶ln, 3 Albert Einstein Strasse', 'Not applicable', NULL, 'The Cologne Cohort of Neutropenic Patients (CoCoNut) - a Non-interventional Cohort Study Assessing Risk Factors, Interventions, and Outcome of Immunosuppressed Patients With or Without Opportunistic Infections', 'http://clinicaltrials.gov/show/NCT01821456', 'Hematological Malignancies', 'neoplasms', 'early', 1, 100, 'Both', 100000, 'anti-infectives', NULL, 'other', NULL, NULL, '', '', NULL, NULL, '', '', NULL, NULL, ''),
('NCT02072057', 6, 7, 'refused', '2020-01-06', '2014-04-20', '2018-11-30', 1685, 'Study of Ruxolitinib in the Treatment of Cachexia in Patients With Tumor-Associated Chronic Wasting Diseases.', 'Division of Hematology/Oncology, University Clinic of Medicine, Kantonsspital Aarau AG, CH-5001 Aarau, Switzerland', 'Switzerland', NULL, '2', 'Nathan Cantoni', 'The RUXexia Trial: An Open-label Phase II Trial of Ruxolitinib in the Treatment of Cachexia in Patients With Tumor-Associated Chronic Wasting Diseases.', 'https://clinicaltrials.gov/show/NCT02072057', 'Cancer Cachexia', 'neoplasms', 'chronic', 18, 100, 'Both', 8, 'ruxolitinib', NULL, 'oral', '0', '100', 'mg/kg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `Email` varchar(255) COLLATE utf8_bin NOT NULL,
  `Pwd` varchar(255) COLLATE utf8_bin NOT NULL,
  `User_Cat` set('moderator','administrator','sponsor','authority','visitor') COLLATE utf8_bin NOT NULL,
  `First_Name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Last_Name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Country` varchar(255) COLLATE utf8_bin NOT NULL,
  `Institut` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`idUser`, `Email`, `Pwd`, `User_Cat`, `First_Name`, `Last_Name`, `Country`, `Institut`) VALUES
(1, 'guillaume@gmail.com', 'pwdmoderator', 'moderator', 'Guillaume', 'Vink', 'Netherlands', NULL),
(2, 'dracau@feu.fr', 'typefeu', 'visitor', 'Dracau', 'Feu', 'Maroc', NULL),
(3, 'loic@hotmail.fr', 'pwdauthority', 'authority', NULL, NULL, 'France', 'Polytech'),
(4, 'cara@puce.fr', 'typeplante', 'visitor', 'Cara', 'Puce', 'France', NULL),
(5, 'manon@free.fr', 'pwdadministrator', 'administrator', 'Manon', 'Carvalho', 'Portugal', NULL),
(6, 'gilles@bernot.fr', 'pwdbernot', 'sponsor', 'Gilles', 'Bernot', 'France', 'CNRS'),
(7, 'bulbi@zarre.fr', 'typeplante', 'visitor', 'Bulbi', 'Zarre', 'France', NULL),
(8, 'benjamin@miraglio.be', 'pwdmiraglio', 'sponsor', 'Benjamin', 'Miraglio', 'Belgium', 'Oncoradiomics\r\n'),
(9, 'jeanpaul@comet.fr', 'pwdcomet', 'sponsor', 'Jean-Paul', 'Comet', 'France', 'UCA');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD UNIQUE KEY `UNIQUE` (`idUser`,`idStudy`),
  ADD KEY `idStudy` (`idStudy`);

--
-- Index pour la table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`dateReport`);

--
-- Index pour la table `restrictionlist`
--
ALTER TABLE `restrictionlist`
  ADD PRIMARY KEY (`idStudy`,`idUser`);

--
-- Index pour la table `restrictions`
--
ALTER TABLE `restrictions`
  ADD PRIMARY KEY (`idRestriction`);

--
-- Index pour la table `studies`
--
ALTER TABLE `studies`
  ADD PRIMARY KEY (`idStudy`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `restrictions`
--
ALTER TABLE `restrictions`
  MODIFY `idRestriction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD CONSTRAINT `bookmarks_ibfk_1` FOREIGN KEY (`idStudy`) REFERENCES `studies` (`idStudy`),
  ADD CONSTRAINT `bookmarks_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `users` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
